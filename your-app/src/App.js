import React, { Component } from 'react';
import { Header,Navigate } from './components';

//import logo from './logo.svg';
//import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Navigate/>
      </div>
    );
  }
}

export default App;
